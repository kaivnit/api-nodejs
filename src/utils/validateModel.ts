import Joi from '@hapi/joi'
import IUserInfo from '../interfaces/userInfo.interface';

export default async function ValidateModel(input: any, model: any): Promise<IUserInfo | any> {
    Joi.validate(input, model, (err, result: IUserInfo) => {
        if (err) {
            throw {
                message: err.details[0].message.replace(/['"]/g, '')
            }
        }
        if (!result) {
            throw new Error('Server Error!')
        } else {
            return result
        }
    })
}