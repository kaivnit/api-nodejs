import { Response } from "express";

// export default function formatResponse(data: any, message: string) {

//     return {
//         data: data,
//         message: message,
//         time: Date.now()
//     }
// }
const defaultMessage = {
    200: 'Success!',
    204: 'No content.',
    400: 'Bad Request.',
    401: 'Unauthorized',
    403: 'Forbidden.',
    404: 'Not Found.',
    500: 'Server Error.',
    502: 'Bad Gateway',
    503: 'Service Unavailable'
}
const formatResponse = (res: Response, statusCode: number, data: any, message?: string) => {
    message = message ? message : ((defaultMessage && defaultMessage[statusCode]) ? defaultMessage[statusCode] : 'Request Fail.');
    return res
        .status(statusCode)
        .send({
            data: data,
            message: message,
            time: Date.now()
        })
}
export default formatResponse;