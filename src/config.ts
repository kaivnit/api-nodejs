const Config = {
    development: {
        host: process.env.HOST || '0.0.0.0',
        port: process.env.PORT || 3000
    },
    production: {
        port: 9000
    },
    saltRounds: 10
}
export default Config;