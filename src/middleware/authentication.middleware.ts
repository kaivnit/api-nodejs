"use strict"

import formatResponse from '../utils/formatResponse';
import JWToken from '../utils/JWToken';

export default class Authentication {
    jWToken = new JWToken();
    checkAuth = async (req, res, next) => {
        try {
            let token = req.headers['x-access-token'] || req.headers['authorization'] || req.query.token || req.body.token;
            if (!token) {
                const message = `Request is not contain Auth token`;
                formatResponse(res, 403, null, message);
            }
            if (token.startsWith('Bearer ')) {
                token = token.slice(7, token.length);
            }
            const resultVerify = await this.jWToken.verifyJWT(token);
            if (!resultVerify) {
                throw Error(`jwt not working.`)
            }
            next();
        } catch (error) {
            formatResponse(res, 500, null);
        }
    }
}