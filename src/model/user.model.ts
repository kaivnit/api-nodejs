import Joi from '@hapi/joi'

const REGEX_EMAIL = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

export const UserModel = Joi.object().keys({
    firstName: Joi.string().required(),
    lastName: Joi.string().required(),
    email: Joi.string().regex(REGEX_EMAIL),
    userName: Joi.string().required(),
    password: Joi.string().required(),
    gender: [Joi.string(), Joi.number()],
    roles: Joi.number().default(2),
    access_token: [Joi.string(), Joi.number()],
    createAt: Joi.date().default(Date.now()),
    updateAt: Joi.date().default(Date.now())
}).without('password', 'access_token')