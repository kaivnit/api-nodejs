export default interface IUserInfo {
    firstName: string,
    lastName: string,
    email: string,
    userName: string,
    password: string,
    gender: number | string,
    access_token: string,
    createAt: Date,
    updateAt: Date
}