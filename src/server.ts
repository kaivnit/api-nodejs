'use strict'

import 'dotenv/config';
import express from 'express';
import { connect } from './db/connectDb';
import bodyParser from 'body-parser';
import cors from 'cors';
import Config from './config';
import router from './routes/api-routes';
import logger from 'morgan';

const app = express();
const environment = process.env.NODE_ENV;
const stage = Config.development;

app.use(cors());
app.use('/uploads', express.static('uploads'));
app.use(bodyParser.urlencoded({
    extended: false
}))
app.use(bodyParser.json())

app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET, PUT, POST, DELETE, OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    next();
});

app.get('/', (req, res) => {
    res.send({
        message: 'Welcome to API server CoderTapSu'
    })
});

app.use('/api', router);

if (environment !== 'production') {
    app.use(logger('dev'));
}

connect((err: any) => {
    if (err) {
        console.log('unable to connect to database');
        process.exit(1);
    } else {
        app.listen(Number(stage.port), stage.host, () => {
            console.log(`Server is running on ${stage.host}:${Number(stage.port)}`)
        })
    }
});