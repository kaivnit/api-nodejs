import path from 'path';
// import ResizeImage from '../middleware/resizeImage';
import formatResponse from '../utils/formatResponse';
import fs from 'fs-extra';
import {
    getDb,
    getPrimaryKey
} from '../db/connectDb';
import archiver from 'archiver';

const collectionImages = 'images';
const collectionFiles = 'files';

const regexCutExt = /\.[^.]{0,4}$/i;


class UploadMediaController {

    deleteImages = async (req, res, next) => {
        try {
            const formData = req.body;
            if (!{
                ...formData
            }.hasOwnProperty('Ids') || Number(Object.keys({
                ...formData
            }).length) !== 1) {
                const message = `Format Error: FormData with key Ids`;
                formatResponse(res, 400, null, message);
            }
            res.status(200).send({
                formData
            });
        } catch (error) {
            formatResponse(res, 500, null, error && error.message);
        }
    }

    uploadOneImage = async (req, res, next) => {
        try {
            if (!req.file) {
                formatResponse(res, 400, null, 'No file.');
                return next();
            }
            // const image = fs.readFileSync(req.file.path);
            // const encode_image = image.toString('base64');
            const finalImg = {
                filename: req.file.originalname.replace(regexCutExt, '').replace(/\s/gm, '_'),
                contentType: req.file.mimetype,
                createAt: Date.now(),
                // image: Buffer.from(encode_image, 'base64'),
                path: req.file.path
            };
            return await getDb().collection(collectionImages).insertOne(finalImg, (err, result) => {
                if (err) {
                    formatResponse(res, 500, null, err && err.message);
                    return next(err);
                }
                const message = `Upload Success!`;
                let {
                    contentType,
                    image,
                    ...y
                } = result.ops[0];
                formatResponse(res, 200, { ...y }, message);
            })
        } catch (error) {
            formatResponse(res, 500, null, error && error.message);
        }
    }

    getImageById = async (req, res, next) => {
        try {
            const id = req.params.id;
            return await getDb().collection(collectionImages).findOne({
                _id: getPrimaryKey(id)
            }, async (err, result) => {
                if (err) {
                    formatResponse(res, 500, null);
                    return next(err);
                }
                if (!result) {
                    formatResponse(res, 500, null, 'File Not Found.');
                    return next(err);
                }
                const message = `Request Successfully`;
                let {
                    contentType,
                    image,
                    ...y
                } = result;
                formatResponse(res, 200, { ...y }, message);
            })
        } catch (error) {
            formatResponse(res, 500, null, error && error.message);
        }
    }

    deleteImageById = async (req, res, next) => {
        try {
            const id = req.params.id;
            const image = await getDb().collection(collectionImages).findOne({
                _id: getPrimaryKey(id)
            });
            await fs.remove(image.path, (err) => {
                if (err) {
                    console.log(err, 'err roi');
                }
                console.log('Deleted File');
            });
            return await getDb().collection(collectionImages).findOneAndDelete({
                _id: getPrimaryKey(id)
            }, async (err, result) => {
                if (err) {
                    formatResponse(res, 500, null, err && err.message);
                    return next(err);
                }
                if (!result) {
                    formatResponse(res, 404, null, 'File Not Found.');
                    return next(err);
                }
                formatResponse(res, 200, null, 'Delete Success!');
            })
        } catch (error) {
            formatResponse(res, 500, null, error && error.message);
        }
    }

    downloadImageById = async (req, res, next) => {
        try {
            const id = req.params.id;
            return await getDb().collection(collectionImages).findOne({
                _id: getPrimaryKey(id)
            }, async (err, result) => {
                if (err) {
                    formatResponse(res, 500, null, err && err.message);
                    return next(err);
                }
                if (!result) {
                    formatResponse(res, 404, null, 'File Not Found.');
                    return next(err);
                }
                // res.contentType('image/jpeg');
                const filename = await `${result.filename}.${result.contentType.match(/([^\/]*)$/i)[0]}`;
                res.contentType(result.contentType);
                res.attachment(filename);
                res.status(200).send(result.image.buffer);
            })
        } catch (error) {
            formatResponse(res, 500, null, error && error.message);
        }
    }

    uploadMultiFiles = async (req, res, next) => {
        try {
            const files = req.files;
            const finalFiles = files.map(fileItem => ({
                filename: fileItem.originalname.replace(regexCutExt, '').replace(/\s/gm, '_'),
                contentType: fileItem.mimetype,
                createAt: Date.now(),
                path: fileItem.path
            }));
            return await getDb().collection(collectionFiles).insertMany(finalFiles, (err, result) => {
                if (err) {
                    formatResponse(res, 500, null, err && err.message);
                }
                const response = result.ops.map(x => ({
                    filename: x.filename,
                    createAt: x.createAt,
                    path: x.path,
                    _id: x._id
                }));
                const message = `Upload Success!`;
                formatResponse(res, 200, response, message);
            })


        } catch (error) {
            formatResponse(res, 500, null, error && error.message);
        }
    }
    downloadZipFiles = async (req, res, next) => {
        try {
            const formData = req.body;
            if (!{
                ...formData
            }.hasOwnProperty('Ids') || Number(Object.keys({
                ...formData
            }).length) !== 1) {
                const message = `Format Error: FormData with key Ids`;
                formatResponse(res, 400, null, message);
            }
            return await getDb().collection(collectionFiles).find({
                _id: {
                    $in: formData.Ids.map(x => getPrimaryKey(x))
                }
            }).toArray(async (err, data) => {
                if (err) {
                    formatResponse(res, 500, null, err && err.message);
                }
                const output = fs.createWriteStream('./uploads/downloadFiles.zip');
                const archive = archiver('zip', {
                    zlib: {
                        level: 9
                    }
                });
                archive.pipe(output);
                data.forEach(x => {
                    const fileItem = path.resolve(__dirname, `../../${x.path}`);
                    archive.append(fs.createReadStream(fileItem), {
                        name: `${x.filename}${x.path.match(regexCutExt)[0]}`
                    });
                });
                output.on('close', function () {
                    res.status(200).download(output.path);
                    // return next();
                });
                archive.finalize();
            })
        } catch (error) {
            formatResponse(res, 500, null, error && error.message);
        }
    }
}

export default new UploadMediaController();