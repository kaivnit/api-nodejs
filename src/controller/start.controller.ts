"use strict"

import LoginService from "../services/login.service";
import formatResponse from "../utils/formatResponse";
import UserService from "../services/user.service";

export default class StartController {
    loginService = new LoginService();
    userService = new UserService();

    signIn = async (req, res) => {
        try {
            this.loginService.login(req, res);
        } catch (error) {
            formatResponse(res, 500, null, error && error.message);
        }
    }

    signUp = async (req, res) => {
        try {
            this.userService.createUser(req, res);
        } catch (error) {
            formatResponse(res, 500, null, error && error.message);
        }
    }
}