'use strict'

import UserService from '../services/user.service';
import formatResponse from '../utils/formatResponse';

export default class UserController {
    userService = new UserService();

    checkExistEmail = async (req, res) => {
        try {
            this.userService.checkExistEmail(req, res);
        } catch (error) {
            formatResponse(res, 500, null);
        }
    }
    checkExistUserName = async (req, res) => {
        try {
            this.userService.checkExistUserName(req, res);
        } catch (error) {
            formatResponse(res, 500, null);
        }
    }

    create = async (req, res) => {
        try {
            this.userService.createUser(req, res)
        } catch (error) {
            formatResponse(res, 500, null, error.message);
        }
    }

    findAll = async (req, res) => {
        try {
            this.userService.fillterUsers(req, res)
        } catch (error) {
            formatResponse(res, 500, null, error.message);

        }
    }

}