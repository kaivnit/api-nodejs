"use strict"

import IUserInfo from '../interfaces/userInfo.interface';
import ValidateModel from '../utils/validateModel';
import { UserModel } from '../model/user.model';
import bcrypt from 'bcrypt';
import Config from '../config';
import formatResponse from '../utils/formatResponse';
import { getDb } from '../db/connectDb';
import { Collection } from 'mongodb';

export default class UserService {
    saltRounds = Config.saltRounds;

    usersCollection = (): Collection<IUserInfo> => getDb().collection('users');

    checkExistEmail = async (req, res) => {
        try {
            const body = req.body;
            const user = await this.usersCollection().findOne({ email: body.email });
            if (user) {
                const result = { isExist: true };
                formatResponse(res, 200, result, 'Email already exists.');
            } else {
                const result = { isExist: false };
                formatResponse(res, 200, result, 'Available.');
            }
        } catch (error) {
            formatResponse(res, 500, null);
        }
    }

    checkExistUserName = async (req, res) => {
        const body = req.body;
        const user = await this.usersCollection().findOne({ userName: body.userName });
        if (user) {
            const result = { isExist: true };
            formatResponse(res, 200, result, 'Username already exists.');
        } else {
            const result = { isExist: false };
            formatResponse(res, 200, result, 'Available.');
        }
    }

    createUser = async (req, res) => {
        const userInfo = req.body;
        const validateInfo = await ValidateModel(userInfo, UserModel);
        const hashPass = await bcrypt.hash(validateInfo.password, this.saltRounds);
        if (!hashPass) {
            formatResponse(res, 500, null);
        }
        const saveUserInfo = {
            ...validateInfo,
            password: hashPass
        };
        const result = await this.usersCollection().insertOne(saveUserInfo);
        if (!result) {
            formatResponse(res, 500, null);
        }
        const { password, ...responseData } = result.ops[0];
        formatResponse(res, 200, responseData);
    }

    replaceUser = async (req, res) => {

    }

    updateUser = async (req, res) => {

    }

    deleteUser = async (req, res) => {

    }

    findUserById = async (req, res) => {

    }

    fillterUsers = async (req, res) => {
        const result = await this.usersCollection()
            .find(
                {},
                {
                    fields: {
                        password: 0,
                        access_token: 0
                    }
                })
            .sort({ createAt: 1 })
            .toArray()
        if (!result) {
            formatResponse(res, 204, null);
        }
        formatResponse(res, 200, result);
    }

}