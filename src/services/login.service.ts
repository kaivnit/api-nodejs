"use strict"

import { Collection } from "mongodb";
import IUserInfo from "../interfaces/userInfo.interface";
import { getDb } from "../db/connectDb";
import formatResponse from "../utils/formatResponse";
import bcrypt from 'bcrypt';
import JWToken from "../utils/JWToken";

export default class LoginService {
    jwToken = new JWToken();

    usersCollection = (): Collection<IUserInfo> => getDb().collection('users');

    login = async (req, res) => {
        const body = req.body;
        const user = await this.usersCollection().findOne({
            $or: [{
                userName: body.userName
            }, {
                email: body.email
            }]
        });
        if (!user) {
            const message = `Invalid UserName or Email`;
            formatResponse(res, 404, null, message);

        }
        const matchPassword = await bcrypt.compare(body.password, user.password);
        if (!matchPassword) {
            const message = `Unauthorized Access. Invalid Password`;
            formatResponse(res, 401, null, message);
        }
        if (matchPassword) {
            const message = `Welcome! ${user.userName}`;
            const access_token = await this.jwToken.createJwt(user);
            await this.usersCollection().updateOne({
                $or: [{
                    userName: body.userName
                }, {
                    email: body.email
                }]
            }, {
                    $set: {
                        access_token: access_token
                    }
                });
            formatResponse(res, 200, {token: access_token}, message);
        } else {
            const message = `Unauthorized Access.`;
            formatResponse(res, 401, null, message);
        }
    }
}