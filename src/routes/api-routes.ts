import { Router } from 'express';
// Import Routes
import UserRoutes from './user.routes';
import UploadMediaRoutes from './uploadMedia.routes';
import StartRoutes from './start.routes';
// **

const router = Router();

router.get('/', (req, res) => {
    res.json({
        status: 'API Its Working',
        message: 'Welcome to RESTHub crafted with love!',
    });
});
// Use Routes
UserRoutes(router);
UploadMediaRoutes(router);
StartRoutes(router);

export default router;