"use strict"

import { Router } from "express";
import StartController from '../controller/start.controller';

const startController = new StartController();

const StartRoutes = (router: Router) => {
    router.route('/signin').post(startController.signIn);
    router.route('signup').post(startController.signUp);
}
export default StartRoutes;