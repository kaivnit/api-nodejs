import UserController from '../controller/user.controller';
import AuthController from '../controller/start.controller';

import Authentication from '../middleware/authentication.middleware';
import { Router } from 'express';

const userController = new UserController();
const authentication = new Authentication();

const UserRoutes = (router: Router) => {
    router.route('/users')
        .get(authentication.checkAuth, userController.findAll);
    // router.route('/users/:id')
    //     .get(userController.findOne)
    //     .post(UserController.findOneGraph);
    //     .put(UserController.update)
    //     .patch(UserController.update)
    //     .delete(UserController.delete)
    router.route('/users/check-email').post(userController.checkExistEmail);
    router.route('/users/check-username').post(userController.checkExistUserName);
}

export default UserRoutes;