# About Project
This project is open. My first NodeJs API build with Express Framework.
Please do not change the project information if you use it! Thank you!

## Installation
### Pre-requirements
Installed MongDb and create a Database
### 1. Clone source code
```
https://gitlab.com/codertapsu/api-nodejs.git
```

### 2. Setup
Create `.env` file in prject root folder with content:
```
JWT_SECRET_KEY=<your_secret_key>
DB_HOST='mongodb://127.0.0.1:27017'
DB_NAME=<your db name>
```
Change DB_HOST value if you have another config

And Install Packages: 
```
yarn install
```


## Usage
Run dev: 
```
yarn run dev
```

Build
```
yarn run build
```
Serve:
```
yarn run serve
```

## COntributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.
Please send me your name to add into prject information!

## License
UNLICENSED

## Contact
My Email:
```
hoangduykhanh21@gmail.com
```

My Skype
```
live:hoangduykhanh21
```
## Project Status
In process